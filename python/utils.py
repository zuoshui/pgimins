#!/usr/bin/python3

import numpy as np
import math


class GNSS_KF_config_class:
    def __init__(self, epoch_interval=0,
                 init_est_r_ea_e=0,
                 no_sat=0,
                 r_os=0,
                 inclination=0,
                 const_delta_lambda=0,
                 const_delta_t=0,
                 mask_angle=0,
                 SIS_err_SD=0,
                 zenith_iono_err_SD=0,
                 code_track_err_SD=0,
                 rate_track_err_SD=0,
                 rx_clock_offset=0,
                 rx_clock_drift=0) -> None:
        self.epoch_interval = epoch_interval
        self.init_est_r_ea_e = init_est_r_ea_e
        self.no_sat = no_sat
        self.r_os = r_os
        self.inclination = inclination
        self.const_delta_lambda = const_delta_lambda
        self.const_delta_t = const_delta_t
        self.mask_angle = mask_angle
        self.SIS_err_SD = SIS_err_SD
        self.zenith_iono_err_SD = zenith_iono_err_SD
        self.code_track_err_SD = code_track_err_SD
        self.rate_track_err_SD = rate_track_err_SD
        self.rx_clock_offset = rx_clock_offset
        self.rx_clock_drift = rx_clock_drift

class IMU_errors_class:
    def __init__(self, delta_r_eb_n=0,
                 b_a=np.matrix([[0.0], [0.0], [0.0]]),
                 b_g=np.matrix([[0.0], [0.0], [0.0]]),
                 M_a=np.matrix(np.eye(3)),
                 M_g=np.matrix(np.eye(3)),
                 G_g=np.matrix(np.eye(3)),
                 accel_noise_root_PSD=0,
                 gyro_noise_root_PSD=0,
                 accel_quant_level=0,
                 gyro_quant_level=0) -> None:
        self.delta_r_eb_n = delta_r_eb_n
        self.b_a = b_a
        self.b_g = b_g
        self.M_a = M_a
        self.M_g = M_g
        self.G_g = G_g
        self.accel_noise_root_PSD = accel_noise_root_PSD
        self.gyro_noise_root_PSD = gyro_noise_root_PSD
        self.accel_quant_level = accel_quant_level
        self.gyro_quant_level = gyro_quant_level


class Initialization_errors_class:
    def __init__(self, delta_r_eb_n=np.matrix([[0.0], [0.0], [0.0]]),
                 delta_v_eb_n=np.matrix([[0.0], [0.0], [0.0]]),
                 delta_eul_nb_n=np.matrix([[0.0], [0.0], [0.0]])) -> None:
        self.delta_r_eb_n = delta_r_eb_n
        self.delta_v_eb_n = delta_v_eb_n
        self.delta_eul_nb_n = delta_eul_nb_n


class TC_KF_config_class:
    def __init__(self, init_att_unc=0,
                 init_vel_unc=0,
                 init_pos_unc=0,
                 init_b_a_unc=0,
                 init_b_g_unc=0,
                 init_clock_offset_unc=0,
                 init_clock_drift_unc=0,
                 gyro_noise_PSD=0,
                 accel_noise_PSD=0,
                 accel_bias_PSD=0,
                 gyro_bias_PSD=0,
                 clock_freq_PSD=0,
                 clock_phase_PSD=0,
                 pseudo_range_SD=0,
                 range_rate_SD=0) -> None:
        self.init_att_unc = init_att_unc
        self.init_vel_unc = init_vel_unc
        self.init_pos_unc = init_pos_unc
        self.init_b_a_unc = init_b_a_unc
        self.init_b_g_unc = init_b_g_unc
        self.init_clock_offset_unc = init_clock_offset_unc
        self.init_clock_drift_unc = init_clock_drift_unc
        self.gyro_noise_PSD = gyro_noise_PSD
        self.accel_noise_PSD = accel_noise_PSD
        self.accel_bias_PSD = accel_bias_PSD
        self.gyro_bias_PSD = gyro_bias_PSD
        self.clock_freq_PSD = clock_freq_PSD
        self.clock_phase_PSD = clock_phase_PSD
        self.pseudo_range_SD = pseudo_range_SD
        self.range_rate_SD = range_rate_SD


class LC_KF_config_class:
    def __init__(self, init_att_unc=0,
                 init_vel_unc=0,
                 init_pos_unc=0,
                 init_b_a_unc=0,
                 init_b_g_unc=0,
                 init_clock_offset_unc=0,
                 init_clock_drift_unc=0,
                 gyro_noise_PSD=0,
                 accel_noise_PSD=0,
                 accel_bias_PSD=0,
                 gyro_bias_PSD=0,
                 clock_freq_PSD=0,
                 clock_phase_PSD=0,
                 pos_meas_SD=0,
                 vel_meas_SD=0) -> None:
        self.init_att_unc = init_att_unc
        self.init_vel_unc = init_vel_unc
        self.init_pos_unc = init_pos_unc
        self.init_b_a_unc = init_b_a_unc
        self.init_b_g_unc = init_b_g_unc
        self.init_clock_offset_unc = init_clock_offset_unc
        self.init_clock_drift_unc = init_clock_drift_unc
        self.gyro_noise_PSD = gyro_noise_PSD
        self.accel_noise_PSD = accel_noise_PSD
        self.accel_bias_PSD = accel_bias_PSD
        self.gyro_bias_PSD = gyro_bias_PSD
        self.clock_freq_PSD = clock_freq_PSD
        self.clock_phase_PSD = clock_phase_PSD
        self.pos_meas_SD = pos_meas_SD
        self.vel_meas_SD = vel_meas_SD


def degtorad(deg):
    return deg * 0.01745329252

def radtodeg(rad):
    return rad / 0.01745329252

# WGS84 Equatorial radius in meters
R_0 = np.float128(6378137)
# WGS84 eccentricity
e = np.float128(0.0818191908425)
ecc2 = np.float128(0.006694380022900)
omega_ie = 7.292115E-5  # Earth rotation rate (rad/s)
c = np.float128(299792458)  # Speed of light in m/s
# Constants
deg_to_rad = np.float128(0.01745329252)
rad_to_deg = 1/deg_to_rad
micro_g_to_meters_per_second_squared = 9.80665E-6
mu = np.float128(3.986004418E14)  # WGS84 Earth gravitational constant (m^3 s^-2)
J_2 = 1.082627E-3  # WGS84 Earth's second gravitational constant
R_P = np.float128(6356752.31425)  # WGS84 Polar radius in meters
f = np.float128(1 / 298.257223563)  # WGS84 flattening
micro_g_to_meters_per_second_squared = 9.80665E-6