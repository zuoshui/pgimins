#!/usr/bin/python3

import numpy as np
import math
import matplotlib.pyplot as plt
from utils import radtodeg

def Plot_errors(errors):
    # Plots navigation solution errors
    #
    # Software for use with "Principles of GNSS, Inertial, and Multisensor
    # Integrated Navigation Systems," Second Edition.
    #
    # This function created 3/4/2012 by Paul Groves
    #
    # Input:
    #   errors      Array of error data to plot
    # Format is
    # Column 1: time (sec)
    # Column 2: north position error (m)
    # Column 3: east position error (m)
    # Column 4: down position error (m)
    # Column 5: north velocity (m/s)
    # Column 6: east velocity (m/s)
    # Column 7: down velocity (m/s)
    # Column 8: roll component of NED attitude error (deg)
    # Column 9: pitch component of NED attitude error (deg)
    # Column 10: yaw component of NED attitude error (deg)

    # Copyright 2012, Paul Groves
    # License: BSD; see license.txt for details

    # Begins

    plt.subplot(3, 3, 1)  # 3行3列，子图plt.subplot('行','列','编号')
    plt.plot(errors[:, 0], errors[:, 1], linewidth=1.5)
    plt.title('North position error in [m]')
    plt.subplot(3, 3, 2)
    plt.plot(errors[:, 0], errors[:, 2], linewidth=1.5)
    plt.title('East position error in [m]')
    plt.subplot(3, 3, 3)
    plt.plot(errors[:, 0], errors[:, 3], linewidth=1.5)
    plt.title('Down position error in [m]')

    plt.subplot(3, 3, 4)
    plt.plot(errors[:, 0], errors[:, 4], linewidth=1.5)
    plt.title('North velocity error in [m]')
    plt.subplot(3, 3, 5)
    plt.plot(errors[:, 0], errors[:, 5], linewidth=1.5)
    plt.title('East velocity error in [m]')
    plt.subplot(3, 3, 6)
    plt.plot(errors[:, 0], errors[:, 6], linewidth=1.5)
    plt.title('Down velocity error in [m]')

    plt.subplot(3, 3, 7)
    plt.plot(errors[:, 0], radtodeg(errors[:, 7]), linewidth=1.5)
    plt.title('Attitude error about North in [degree]')
    plt.subplot(3, 3, 8)
    plt.plot(errors[:, 0], radtodeg(errors[:, 8]), linewidth=1.5)
    plt.title('Attitude error about East in [degree]')
    plt.subplot(3, 3, 9)
    plt.plot(errors[:, 0], radtodeg(errors[:, 9]), linewidth=1.5)
    plt.title('Heading error in [degree]')

    plt.show()
