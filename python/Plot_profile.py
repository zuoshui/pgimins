#!/usr/bin/python3

import numpy as np
import math
import matplotlib.pyplot as plt
from Radii_of_curvature import *
from utils import radtodeg, degtorad


def Plot_profile(profile):
    # Plots a motion profile
    #
    # Software for use with "Principles of GNSS, Inertial, and Multisensor
    # Integrated Navigation Systems," Second Edition.
    #
    # This function created 3/4/2012 by Paul Groves
    #
    # Input:
    #   profile      Array of motion profile data to plot
    # Format is
    # Column 1: time (sec)
    # Column 2: latitude (deg)
    # Column 3: longitude (deg)
    # Column 4: height (m)
    # Column 5: north velocity (m/s)
    # Column 6: east velocity (m/s)
    # Column 7: down velocity (m/s)
    # Column 8: roll angle of body w.r.t NED (deg)
    # Column 9: pitch angle of body w.r.t NED (deg)
    # Column 10: yaw angle of body w.r.t NED (deg)

    # Copyright 2012, Paul Groves
    # License: BSD; see license.txt for details

    # Begins
    # fig = figure;
    # set(fig,'units','normalized');
    # set(fig,'OuterPosition',[0.05,0.4,0.45,0.6]);

    R_N, R_E = Radii_of_curvature(profile[0, 1])

    plt.subplot(3, 3, 1)  # 3行3列，子图plt.subplot('行','列','编号')
    plt.plot(profile[:, 0], (profile[:, 1] - profile[0, 1])
             * (R_N + profile[0, 3]), linewidth=1.5)
    plt.title('North displacement in [m]')
    plt.subplot(3, 3, 2)
    plt.plot(profile[:, 0], (profile[:, 2] - profile[0, 2])
             * (R_N + profile[0, 3]) * np.cos(profile[0, 1]), linewidth=1.5)
    plt.title('East displacement in [m]')
    plt.subplot(3, 3, 3)
    plt.plot(profile[:, 0], (profile[0, 3] - profile[:, 3]), linewidth=1.5)
    plt.title('Down displacement in [m]')

    plt.subplot(3, 3, 4)
    plt.plot(profile[:, 0], profile[:, 4], linewidth=1.5)
    plt.title('North velocity in [m]')
    plt.subplot(3, 3, 5)
    plt.plot(profile[:, 0], profile[:, 5], linewidth=1.5)
    plt.title('East velocity in [m]')
    plt.subplot(3, 3, 6)
    plt.plot(profile[:, 0], profile[:, 6], linewidth=1.5)
    plt.title('Down velocity in [m]')

    plt.subplot(3, 3, 7)
    plt.plot(profile[:, 0], radtodeg(profile[:, 7]), linewidth=1.5)
    plt.title('Bank in [degree]')
    plt.subplot(3, 3, 8)
    plt.plot(profile[:, 0], radtodeg(profile[:, 8]), linewidth=1.5)
    plt.title('Elevation in [degree]')
    plt.subplot(3, 3, 9)
    plt.plot(profile[:, 0], radtodeg(profile[:, 9]), linewidth=1.5)
    plt.title('Heading in [degree]')

    plt.show()
