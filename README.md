# PGIMINS

This repo serves as the appendix code for "Principles of GNSS, Inertial, and Multisensor Integrated Navigation Systems," Second Edition by Paul Groves, and could be redistributed under BSD License (see LISENCE file for details). It is originally implemented in MATLAB by Paul Groves, and ported to Python by Zuoshui Zhou, of course, with same license.

## References

* Paul Groves, Principles of GNSS, Inertial, and Multisensor Integrated Navigation Systems, second edition, 2012
* The MATLAB code is from [this GitHub repo](https://github.com/zbai/MATLAB-Groves)
