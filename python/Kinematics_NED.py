#!/usr/bin/python3

import numpy as np
import math

from Gravity_NED import *
from Radii_of_curvature import *
from Skew_symmetric import *
from utils import omega_ie


def Kinematics_NED(tor_i, C_b_n, old_C_b_n, v_eb_n, old_v_eb_n, L_b, h_b, old_L_b, old_h_b):
    # Kinematics_NED - calculates specific force and angular rate from input
    # w.r.t and resolved along north, east, and down
    #
    # Software for use with "Principles of GNSS, Inertial, and Multisensor
    # Integrated Navigation Systems," Second Edition.
    #
    # This function created 1/4/2012 by Paul Groves
    #
    # Inputs:
    #   tor_i         time interval between epochs (s)
    #   C_b_n         body-to-NED coordinate transformation matrix
    #   old_C_b_n     previous body-to-NED coordinate transformation matrix
    #   v_eb_n        velocity of body frame w.r.t. ECEF frame, resolved along
    #                 north, east, and down (m/s)
    #   old_v_eb_n    previous velocity of body frame w.r.t. ECEF frame, resolved
    #                 along north, east, and down (m/s)
    #   L_b           latitude (rad)
    #   h_b           height (m)
    #   old_L_b       previous latitude (rad)
    #   old_h_b       previous height (m)
    # Outputs:
    #   f_ib_b        specific force of body frame w.r.t. ECEF frame, resolved
    #                 along body-frame axes, averaged over time interval (m/s^2)
    #   omega_ib_b    angular rate of body frame w.r.t. ECEF frame, resolved
    #                 about body-frame axes, averaged over time interval (rad/s)

    # Copyright 2012, Paul Groves
    # License: BSD; see license.txt for details

    # Begins
    omega_ib_b = np.matrix(np.zeros((3,1)))
    f_ib_b = np.matrix(np.zeros((3,1)))

    if tor_i > 0:

        # From (2.123) , determine the angular rate of the ECEF frame
        # w.r.t the ECI frame, resolved about NED
        omega_ie_n = omega_ie * \
            np.matrix([np.cos(old_L_b), 0.0, - np.sin(old_L_b)]).transpose()

        # From (5.44), determine the angular rate of the NED frame
        # w.r.t the ECEF frame, resolved about NED
        old_R_N, old_R_E = Radii_of_curvature(old_L_b)
        R_N, R_E = Radii_of_curvature(L_b)
        old_omega_en_n = np.matrix([[old_v_eb_n[1, 0] / (old_R_E + old_h_b)],
                                    [-old_v_eb_n[0, 0] / (old_R_N + old_h_b)],
                                    [-old_v_eb_n[1, 0] * np.tan(old_L_b) / (old_R_E + old_h_b)]], dtype=np.float64)
        omega_en_n = np.matrix([[v_eb_n[1, 0] / (R_E + h_b)],
                                [-v_eb_n[0, 0] / (R_N + h_b)],
                                [-v_eb_n[1, 0] * np.tan(L_b) / (R_E + h_b)]], dtype=np.float64)

        # Obtain coordinate transformation matrix from the old attitude (w.r.t.
        # an inertial frame) to the new using (5.77)
        C_old_new = C_b_n.transpose() * (np.matrix(np.eye(3)) - Skew_symmetric(omega_ie_n +
                                                                               0.5 * omega_en_n + 0.5 * old_omega_en_n) * tor_i) * old_C_b_n

        # Calculate the approximate angular rate w.r.t. an inertial frame
        alpha_ib_b = np.matrix(np.zeros((3,1)))
        alpha_ib_b[0, 0] = 0.5 * (C_old_new[1, 2] - C_old_new[2, 1])
        alpha_ib_b[1, 0] = 0.5 * (C_old_new[2, 0] - C_old_new[0, 2])
        alpha_ib_b[2, 0] = 0.5 * (C_old_new[0, 1] - C_old_new[1, 0])

        # Calculate and apply the scaling factor
        temp = np.arccos(
            0.5 * (C_old_new[0, 0] + C_old_new[1, 1] + C_old_new[2, 2] - 1.0))
        if temp > 2e-5:  # scaling is 1 if temp is less than this
            alpha_ib_b = alpha_ib_b * temp/np.sin(temp)

        # Calculate the angular rate
        omega_ib_b = alpha_ib_b / tor_i

        # Calculate the specific force resolved about ECEF-frame axes
        # From (5.54),
        f_ib_n = ((v_eb_n - old_v_eb_n) / tor_i) - Gravity_NED(old_L_b, old_h_b) \
            + Skew_symmetric(old_omega_en_n + 2 * omega_ie_n) * old_v_eb_n

        # Calculate the average body-to-NED coordinate transformation
        # matrix over the update interval using (5.84) and (5.86)
        mag_alpha = math.sqrt(alpha_ib_b.transpose() * alpha_ib_b)
        Alpha_ib_b = Skew_symmetric(alpha_ib_b)

        if mag_alpha > 1.E-8:
            ave_C_b_n = old_C_b_n * (np.matrix(np.eye(3)) + (1 - np.cos(mag_alpha)) / math.pow(mag_alpha, 2)
                                     * Alpha_ib_b + (1 - np.sin(mag_alpha) / mag_alpha) / math.pow(mag_alpha, 2)
                                     * Alpha_ib_b * Alpha_ib_b) - \
                0.5 * Skew_symmetric(old_omega_en_n + omega_ie_n) * old_C_b_n
        else:
            ave_C_b_n = old_C_b_n - 0.5 * \
                Skew_symmetric(old_omega_en_n + omega_ie_n) * old_C_b_n

        # Transform specific force to body-frame resolving axes using (5.81)
        f_ib_b = np.linalg.inv(ave_C_b_n) * f_ib_n

        # If time interval is zero, set angular rate and specific force to zero

    return f_ib_b, omega_ib_b
