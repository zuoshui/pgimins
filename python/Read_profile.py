#!/usr/bin/python3

import numpy as np
import math
import pandas as pd
from utils import deg_to_rad

def Read_profile(filename):
    # Read_profile - inputs a motion profile in the following .csv format
    # Column 1: time (sec)
    # Column 2: latitude (deg)
    # Column 3: longitude (deg)
    # Column 4: height (m)
    # Column 5: north velocity (m/s)
    # Column 6: east velocity (m/s)
    # Column 7: down velocity (m/s)
    # Column 8: roll angle of body w.r.t NED (deg)
    # Column 9: pitch angle of body w.r.t NED (deg)
    # Column 10: yaw angle of body w.r.t NED (deg)
    #
    # Software for use with "Principles of GNSS, Inertial, and Multisensor
    # Integrated Navigation Systems," Second Edition.
    #
    # This function created 31/3/2012 by Paul Groves
    #
    # Inputs:
    #   filename     Name of file to write
    #
    # Outputs:
    #   in_profile   Array of data from the file
    #   no_epochs    Number of epochs of data in the file
    #   ok           Indicates file has the expected number of columns

    # Copyright 2012, Paul Groves
    # License: BSD; see license.txt for details

    # Begins

    # Read in the profile in .csv format
    data = np.matrix(pd.read_csv(filename, delimiter=',', index_col=False, header=None))
    (rows, cols) = data.shape

    if cols != 10:
        return False, False, False

    for r in range(rows):
        for j in [1, 2, 7, 8, 9]:
            data[r, j] *= deg_to_rad

    return data, rows, True
