#!/usr/bin/python3

import numpy as np
import math

from Skew_symmetric import *
from utils import c, omega_ie


def GNSS_LS_position_velocity(GNSS_measurements, no_GNSS_meas, predicted_r_ea_e, predicted_v_ea_e):
    # GNSS_LS_position_velocity - Calculates position, velocity, clock offset,
    # and clock drift using unweighted iterated least squares. Separate
    # calculations are implemented for position and clock offset and for
    # velocity and clock drift
    #
    # Software for use with "Principles of GNSS, Inertial, and Multisensor
    # Integrated Navigation Systems," Second Edition.
    #
    # This function created 11/4/2012 by Paul Groves
    #
    # Inputs:
    #   GNSS_measurements     GNSS measurement data:
    #     Column 1              Pseudo-range measurements (m)
    #     Column 2              Pseudo-range rate measurements (m/s)
    #     Columns 3-5           Satellite ECEF position (m)
    #     Columns 6-8           Satellite ECEF velocity (m/s)
    #   no_GNSS_meas          Number of satellites for which measurements are
    #                         supplied
    #   predicted_r_ea_e      prior predicted ECEF user position (m)
    #   predicted_v_ea_e      prior predicted ECEF user velocity (m/s)
    #
    # Outputs:
    #   est_r_ea_e            estimated ECEF user position (m)
    #   est_v_ea_e            estimated ECEF user velocity (m/s)
    #   est_clock             estimated receiver clock offset (m) and drift (m/s)

    # Copyright 2012, Paul Groves
    # License: BSD; see license.txt for details

    # Begins

    # POSITION AND CLOCK OFFSET
    x_pred = np.matrix(np.zeros((4,1)))
    pred_meas = np.matrix(np.zeros((no_GNSS_meas, 1)))
    H_matrix = np.matrix(np.zeros((no_GNSS_meas, 4)))
    est_r_ea_e = np.matrix(np.zeros((3,1))) 
    est_v_ea_e = np.matrix(np.zeros((3,1)))
    est_clock = np.matrix([0.0, 0.0])

    # Setup predicted state
    x_pred[0:3, 0] = predicted_r_ea_e
    x_pred[3, 0] = 0
    test_convergence = 1

    # Repeat until convergence
    while test_convergence > 0.0001:

        # Loop measurements
        for j in range(no_GNSS_meas):

            # Predict approx range
            delta_r = GNSS_measurements[j, 2:5].transpose() - x_pred[0:3, 0]
            approx_range = math.sqrt(delta_r.transpose() * delta_r)

            # Calculate frame rotation during signal transit time using (8.36)
            C_e_I = np.matrix([[1.0,                                      omega_ie * approx_range / c,         0.0],   \
                               [-omega_ie * approx_range / c,            1.0,                                     0.0],   \
                               [0.0,                                         0.0,                                     1.0]])

            # Predict pseudo-range using (9.143)
            delta_r = C_e_I * GNSS_measurements[j, 2:5].transpose() - x_pred[0:3, 0]
            Range = math.sqrt(delta_r.transpose() * delta_r)
            pred_meas[j, 0] = Range + x_pred[3, 0]

            # Predict line of sight and deploy in measurement matrix, (9.144)
            H_matrix[j, 0:3] = - delta_r.transpose() / Range
            H_matrix[j, 3] = 1

        # Unweighted least-squares solution, (9.35)/(9.141)
        x_est = x_pred + np.linalg.inv(H_matrix[0:no_GNSS_meas, :].transpose() * H_matrix[0:no_GNSS_meas, :]) * \
            H_matrix[0:no_GNSS_meas, :].transpose() * (GNSS_measurements[0:no_GNSS_meas, 0] - pred_meas[0:no_GNSS_meas, 0])

        # Test convergence
        test_convergence = math.sqrt((x_est - x_pred).transpose() * (x_est - x_pred))

        # Set predictions to estimates for next iteration
        x_pred = x_est

    # Set outputs to estimates
    est_r_ea_e[0:3, 0] = x_est[0:3, 0]
    est_clock[0, 0] = x_est[3, 0]

    # VELOCITY AND CLOCK DRIFT

    # Skew symmetric matrix of Earth rate
    Omega_ie = Skew_symmetric(np.matrix([0.0, 0.0, omega_ie]).transpose())

    # Setup predicted state
    x_pred[0:3, 0] = predicted_v_ea_e
    x_pred[3, 0] = 0
    test_convergence = 1

    # Repeat until convergence
    while test_convergence > 0.0001:

        # Loop measurements
        for j in range(no_GNSS_meas):

            # Predict approx range
            delta_r = GNSS_measurements[j, 2:5].transpose() - est_r_ea_e
            approx_range = math.sqrt(delta_r.transpose() * delta_r)

            # Calculate frame rotation during signal transit time using (8.36)
            C_e_I = np.matrix([[1.0,                                      omega_ie * approx_range / c,       0.0],    
                               [-omega_ie * approx_range / c,          1.0,                                     0.0],      
                               [0.0,                                       0.0,                                     1.0]])

            # Calculate range using (8.35)
            delta_r = C_e_I * GNSS_measurements[j, 2:5].transpose() - est_r_ea_e
            Range = math.sqrt(delta_r.transpose() * delta_r)

            # Calculate line of sight using (8.41)
            u_as_e = delta_r / Range

            # Predict pseudo-range rate using (9.143)
            range_rate = u_as_e.transpose() * (C_e_I * (GNSS_measurements[j, 5:8].transpose() + \
                                                        Omega_ie * GNSS_measurements[j, 2:5].transpose()) - (x_pred[0:3, 0] + Omega_ie * est_r_ea_e))
            pred_meas[j, 0] = range_rate + x_pred[3, 0]

            # Predict line of sight and deploy in measurement matrix, (9.144)
            H_matrix[j, 0:3] = - u_as_e.transpose()
            H_matrix[j, 3] = 1

        # Unweighted least-squares solution, (9.35)/(9.141)
        x_est = x_pred + np.linalg.inv(H_matrix[0:no_GNSS_meas, :].transpose() * H_matrix[0:no_GNSS_meas, :]) * H_matrix[0:no_GNSS_meas, :].transpose() * \
            (GNSS_measurements[0:no_GNSS_meas, 1] - pred_meas[0:no_GNSS_meas, 0])

        # Test convergence
        test_convergence = math.sqrt((x_est - x_pred).transpose() * (x_est - x_pred))

        # Set predictions to estimates for next iteration
        x_pred = x_est

    # Set outputs to estimates
    est_v_ea_e[0:3, 0] = x_est[0:3, 0]
    est_clock[0, 1] = x_est[3, 0]

    return est_r_ea_e, est_v_ea_e, est_clock
