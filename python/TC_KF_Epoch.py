#!/usr/bin/python3

import numpy as np
import math

from Skew_symmetric import *
from Gravity_ECEF import *
from utils import R_0, e, omega_ie, c

def TC_KF_Epoch(GNSS_measurements, no_meas, tor_s, est_C_b_e_old, est_v_eb_e_old, est_r_eb_e_old,
                est_IMU_bias_old, est_clock_old, P_matrix_old, meas_f_ib_b, est_L_b_old, TC_KF_config):
    # TC_KF_Epoch - Implements one cycle of the tightly coupled INS/GNSS
    # extended Kalman filter plus closed-loop correction of all inertial states
    #
    # Software for use with "Principles of GNSS, Inertial, and Multisensor
    # Integrated Navigation Systems," Second Edition.
    #
    # This function created 12/4/2012 by Paul Groves
    #
    # Inputs:
    #   GNSS_measurements     GNSS measurement data:
    #     Column 1              Pseudo-range measurements (m)
    #     Column 2              Pseudo-range rate measurements (m/s)
    #     Columns 3-5           Satellite ECEF position (m)
    #     Columns 6-8           Satellite ECEF velocity (m/s)
    #   no_meas               Number of satellites for which measurements are
    #                         supplied
    #   tor_s                 propagation interval (s)
    #   est_C_b_e_old         prior estimated body to ECEF coordinate
    #                         transformation matrix
    #   est_v_eb_e_old        prior estimated ECEF user velocity (m/s)
    #   est_r_eb_e_old        prior estimated ECEF user position (m)
    #   est_IMU_bias_old      prior estimated IMU biases (body axes)
    #   est_clock_old         prior Kalman filter state estimates
    #   P_matrix_old          previous Kalman filter error covariance matrix
    #   meas_f_ib_b           measured specific force
    #   est_L_b_old           previous latitude solution
    #   TC_KF_config
    #     .gyro_noise_PSD     Gyro noise PSD (rad^2/s)
    #     .accel_noise_PSD    Accelerometer noise PSD (m^2 s^-3)
    #     .accel_bias_PSD     Accelerometer bias random walk PSD (m^2 s^-5)
    #     .gyro_bias_PSD      Gyro bias random walk PSD (rad^2 s^-3)
    #     .clock_freq_PSD     Receiver clock frequency-drift PSD (m^2/s^3)
    #     .clock_phase_PSD    Receiver clock phase-drift PSD (m^2/s)
    #     .pseudo_range_SD    Pseudo-range measurement noise SD (m)
    #     .range_rate_SD      Pseudo-range rate measurement noise SD (m/s)
    #
    # Outputs:
    #   est_C_b_e_new     updated estimated body to ECEF coordinate
    #                      transformation matrix
    #   est_v_eb_e_new    updated estimated ECEF user velocity (m/s)
    #   est_r_eb_e_new    updated estimated ECEF user position (m)
    #   est_IMU_bias_new  updated estimated IMU biases
    #     Rows 1-3          estimated accelerometer biases (m/s^2)
    #     Rows 4-6          estimated gyro biases (rad/s)
    #   est_clock_new     updated Kalman filter state estimates
    #     Row 1             estimated receiver clock offset (m)
    #     Row 2             estimated receiver clock drift (m/s)
    #   P_matrix_new      updated Kalman filter error covariance matrix

    # Copyright 2012, Paul Groves
    # License: BSD; see license.txt for details

    # Begins

    # Skew symmetric matrix of Earth rate
    Omega_ie = Skew_symmetric(np.matrix([0.0, 0.0, omega_ie]).transpose())

    # SYSTEM PROPAGATION PHASE

    # 1. Determine transition matrix using (14.50) (first-order approx)
    Phi_matrix = np.matrix(np.eye(17))
    Phi_matrix[0:3, 0:3] = Phi_matrix[0:3, 0:3] - Omega_ie * tor_s
    Phi_matrix[0:3, 12:15] = est_C_b_e_old * tor_s
    Phi_matrix[3:6, 0:3] = -tor_s * Skew_symmetric(est_C_b_e_old * meas_f_ib_b)
    Phi_matrix[3:6, 3:6] = Phi_matrix[3:6, 3:6] - 2 * Omega_ie * tor_s
    geocentric_radius = R_0 / math.sqrt(1 - (e * math.pow(np.sin(est_L_b_old), 2))) * \
        math.sqrt(math.pow(np.cos(est_L_b_old), 2) + math.pow(1 - e*e, 2) \
                   * math.pow(np.sin(est_L_b_old), 2))  # from (2.137)
    Phi_matrix[3:6, 6:9] = -tor_s * 2 * Gravity_ECEF(est_r_eb_e_old) / \
        geocentric_radius * est_r_eb_e_old.transpose() / \
        math.sqrt(est_r_eb_e_old.transpose() * est_r_eb_e_old)
    Phi_matrix[3:6, 9:12] = est_C_b_e_old * tor_s
    Phi_matrix[6:9, 3:6] = np.matrix(np.eye(3)) * tor_s
    Phi_matrix[15, 16] = tor_s

    # 2. Determine approximate system noise covariance matrix using (14.82)
    Q_prime_matrix = np.matrix(np.zeros((17, 17)))
    Q_prime_matrix[0:3, 0:3] = np.matrix(
        np.eye(3)) * TC_KF_config.gyro_noise_PSD * tor_s
    Q_prime_matrix[3:6, 3:6] = np.matrix(
        np.eye(3)) * TC_KF_config.accel_noise_PSD * tor_s
    Q_prime_matrix[9:12, 9:12] = np.matrix(
        np.eye(3)) * TC_KF_config.accel_bias_PSD * tor_s
    Q_prime_matrix[12:15, 12:15] = np.matrix(
        np.eye(3)) * TC_KF_config.gyro_bias_PSD * tor_s
    Q_prime_matrix[15, 15] = TC_KF_config.clock_phase_PSD * tor_s
    Q_prime_matrix[16, 16] = TC_KF_config.clock_freq_PSD * tor_s

    # 3. Propagate state estimates using (3.14) noting that only the clock
    # states are non-zero due to closed-loop correction.
    x_est_propagated = np.matrix(np.zeros((17, 1)))
    x_est_propagated[15, 0] = est_clock_old[0, 0] + est_clock_old[0, 1] * tor_s
    x_est_propagated[16, 0] = est_clock_old[0, 1]

    # 4. Propagate state estimation error covariance matrix using (3.46)
    P_matrix_propagated = Phi_matrix * \
        (P_matrix_old + 0.5 * Q_prime_matrix) * \
        Phi_matrix.transpose() + 0.5 * Q_prime_matrix

    # MEASUREMENT UPDATE PHASE

    u_as_e_T = np.matrix(np.zeros((no_meas, 3)))
    pred_meas = np.matrix(np.zeros((no_meas, 2)))

    # Loop measurements
    for j in range(no_meas):

        # Predict approx range
        delta_r = GNSS_measurements[j, 2:5].transpose() - est_r_eb_e_old
        approx_range = math.sqrt(delta_r.transpose() * delta_r)

        # Calculate frame rotation during signal transit time using (8.36)
        C_e_I = np.matrix([[1.0,                                   omega_ie * approx_range / c,    0.0],
                           [-omega_ie * approx_range / c,
                               1.0,                                  0.0],
                           [0.0,                                    0.0,                                  1.0]])

        # Predict pseudo-range using (9.165)
        delta_r = C_e_I * GNSS_measurements[j,
                                            2:5].transpose() - est_r_eb_e_old
        Range = math.sqrt(delta_r.transpose() * delta_r)
        pred_meas[j, 0] = Range + x_est_propagated[15, 0]

        # Predict line of sight
        u_as_e_T[j, 0:3] = delta_r.transpose() / Range

        # Predict pseudo-range rate using (9.165)
        range_rate = u_as_e_T[j, 0:3] * (C_e_I * (GNSS_measurements[j, 5:8].transpose() + Omega_ie * GNSS_measurements[j, 2:5].transpose())
                                         - (est_v_eb_e_old + Omega_ie * est_r_eb_e_old))
        pred_meas[j, 1] = range_rate + x_est_propagated[16, 0]

    # 5. Set-up measurement matrix using (14.126)
    H_matrix = np.matrix(np.zeros((2*no_meas, 17)))
    H_matrix[0:no_meas, 6:9] = u_as_e_T[0:no_meas, 0:3]
    H_matrix[0:no_meas, 15] = np.matrix(np.ones((no_meas, 1)))
    H_matrix[no_meas:(2*no_meas), 3:6] = u_as_e_T[0:no_meas, 0:3]
    H_matrix[no_meas:(2*no_meas), 16] = np.matrix(np.ones((no_meas, 1)))

    # 6. Set-up measurement noise covariance matrix assuming all measurements
    # are independent and have equal variance for a given measurement type.
    R_matrix = np.matrix(np.zeros((2*no_meas, 2*no_meas)))
    R_matrix[0:no_meas, 0:no_meas] = np.matrix(
        np.eye(no_meas)) * math.pow(TC_KF_config.pseudo_range_SD, 2)
    R_matrix[0:no_meas, no_meas:(2 * no_meas)] = np.matrix(np.zeros(no_meas))
    R_matrix[no_meas:(2*no_meas), 0:no_meas] = np.matrix(np.zeros(no_meas))
    R_matrix[no_meas:(2*no_meas), no_meas:(2*no_meas)] = np.matrix(
        np.eye(no_meas)) * math.pow(TC_KF_config.range_rate_SD, 2)

    # 7. Calculate Kalman gain using (3.21)
    K_matrix = P_matrix_propagated * H_matrix.transpose() * np.linalg.inv(H_matrix *
                                                                          P_matrix_propagated * H_matrix.transpose() + R_matrix)

    # 8. Formulate measurement innovations using (14.119)
    delta_z = np.matrix(np.zeros((2 * no_meas, 1)))
    delta_z[0:no_meas, 0] = GNSS_measurements[0:no_meas, 0] - \
        pred_meas[0:no_meas, 0]
    delta_z[no_meas:(2*no_meas), 0] = GNSS_measurements[0:no_meas,
                                                        1] - pred_meas[0:no_meas, 1]

    # 9. Update state estimates using (3.24)
    x_est_new = x_est_propagated + K_matrix * delta_z

    # 10. Update state estimation error covariance matrix using (3.25)
    P_matrix_new = (np.matrix(np.eye(17)) - K_matrix *
                    H_matrix) * P_matrix_propagated

    # CLOSED-LOOP CORRECTION

    # Correct attitude, velocity, and position using (14.7-9)
    est_C_b_e_new = (np.matrix(np.eye(3)) -
                     Skew_symmetric(x_est_new[0:3, 0])) * est_C_b_e_old
    est_v_eb_e_new = est_v_eb_e_old - x_est_new[3:6, 0]
    est_r_eb_e_new = est_r_eb_e_old - x_est_new[6:9, 0]

    # Update IMU bias and GNSS receiver clock estimates
    est_IMU_bias_new = est_IMU_bias_old + x_est_new[9:15, 0]
    est_clock_new = x_est_new[15:17, 0].transpose()

    return est_C_b_e_new, est_v_eb_e_new, est_r_eb_e_new, est_IMU_bias_new, est_clock_new, P_matrix_new
