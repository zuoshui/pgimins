#!/usr/bin/python3

import numpy as np
import math


def Skew_symmetric(a):
    # Skew_symmetric - Calculates skew-symmetric matrix
    #
    # Software for use with "Principles of GNSS, Inertial, and Multisensor
    # Integrated Navigation Systems," Second Edition.
    #
    # This function created 1/4/2012 by Paul Groves
    #
    # Inputs:
    #   a       3-element vector, here in form of matrix 3x1
    # Outputs:
    #   A       3x3matrix

    # Copyright 2012, Paul Groves
    # License: BSD; see license.txt for details

    # Begins

    A = np.matrix([[0.0,        -a[2, 0],  a[1, 0]],
                   [a[2, 0],   0.0,        -a[0, 0]],
                   [-a[1, 0],  a[0, 0],     0.0]])
    return A
