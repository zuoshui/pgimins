#!/usr/bin/python3

import numpy as np
import math


def IMU_model(tor_i, true_f_ib_b, true_omega_ib_b, IMU_errors, old_quant_residuals):
    # IMU_model - Simulates an inertial measurement unit (IMU body axes used
    # throughout this function)
    #
    # Software for use with "Principles of GNSS, Inertial, and Multisensor
    # Integrated Navigation Systems," Second Edition.
    #
    # This function created 3/4/2012 by Paul Groves
    #
    # Inputs:
    #   tor_i            time interval between epochs (s)
    #   true_f_ib_b      true specific force of body frame w.r.t. ECEF frame, resolved
    #                    along body-frame axes, averaged over time interval (m/s^2)
    #   true_omega_ib_b  true angular rate of body frame w.r.t. ECEF frame, resolved
    #                    about body-frame axes, averaged over time interval (rad/s)
    #   IMU_errors
    #     .delta_r_eb_n     position error resolved along NED (m)
    #     .b_a              Accelerometer biases (m/s^2)
    #     .b_g              Gyro biases (rad/s)
    #     .M_a              Accelerometer scale factor and cross coupling errors
    #     .M_g              Gyro scale factor and cross coupling errors
    #     .G_g              Gyro g-dependent biases (rad-sec/m)
    #     .accel_noise_root_PSD   Accelerometer noise root PSD (m s^-1.5)
    #     .gyro_noise_root_PSD    Gyro noise root PSD (rad s^-0.5)
    #     .accel_quant_level      Accelerometer quantization level (m/s^2)
    #     .gyro_quant_level       Gyro quantization level (rad/s)
    #   old_quant_residuals  residuals of previous output quantization process
    #
    # Outputs:
    #   meas_f_ib_b      output specific force of body frame w.r.t. ECEF frame, resolved
    #                    along body-frame axes, averaged over time interval (m/s^2)
    #   meas_omega_ib_b  output angular rate of body frame w.r.t. ECEF frame, resolved
    #                    about body-frame axes, averaged over time interval (rad/s)
    #   quant_residuals  residuals of output quantization process

    # Copyright 2012, Paul Groves
    # License: BSD; see license.txt for details

    # Begins

    # Generate noise
    if tor_i > 0:
        accel_noise = np.random.normal(loc=0, scale=1.0, size=(
            3, 1)) * IMU_errors.accel_noise_root_PSD / math.sqrt(tor_i)
        gyro_noise = np.random.normal(loc=0, scale=1.0, size=(
            3, 1)) * IMU_errors.gyro_noise_root_PSD / math.sqrt(tor_i)
    else:
        accel_noise = np.matrix(np.zeros((3,1)))
        gyro_noise = np.matrix(np.zeros((3,1)))

    # Calculate accelerometer and gyro outputs using (4.16) and (4.17)
    uq_f_ib_b = IMU_errors.b_a + \
        (np.matrix(np.eye(3)) + IMU_errors.M_a) * true_f_ib_b + accel_noise
    uq_omega_ib_b = IMU_errors.b_g + (np.matrix(np.eye(3)) + IMU_errors.M_g) * \
        true_omega_ib_b + IMU_errors.G_g * true_f_ib_b + gyro_noise

    quant_residuals = np.matrix(np.zeros((6, 1)))
    # Quantize accelerometer outputs
    if IMU_errors.accel_quant_level > 0:
        meas_f_ib_b = IMU_errors.accel_quant_level * \
            np.round_(
                ((uq_f_ib_b + old_quant_residuals[0:3, 0])) / IMU_errors.accel_quant_level)
        quant_residuals[0:3, 0] = uq_f_ib_b + \
            old_quant_residuals[0:3, 0] - meas_f_ib_b
    else:
        meas_f_ib_b = uq_f_ib_b
        quant_residuals[0:3, 0] = np.matrix(np.zeros((3,1)))

    # Quantize gyro outputs
    if IMU_errors.gyro_quant_level > 0:
        meas_omega_ib_b = IMU_errors.gyro_quant_level * \
            np.round_(
                ((uq_omega_ib_b + old_quant_residuals[3:6, 0])) / IMU_errors.gyro_quant_level)
        quant_residuals[3:6, 0] = uq_omega_ib_b + \
            old_quant_residuals[3:6, 0] - meas_omega_ib_b
    else:
        meas_omega_ib_b = uq_omega_ib_b
        quant_residuals[3:6, 0] = np.matrix(np.zeros((3,1)))

    return meas_f_ib_b, meas_omega_ib_b, quant_residuals
